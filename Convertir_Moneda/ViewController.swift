//
//  ViewController.swift
//  Convertir_Moneda
//
//  Created by Pablo  on 05/08/2019.
//  Copyright © 2019 Pablo Benzo. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    
    
    @IBOutlet var montoIngreso: UITextField!
    
    @IBOutlet var monOrigen: UISegmentedControl!
    
    @IBOutlet var monDestino: UISegmentedControl!
    
    @IBOutlet var resuLabel: UILabel!
    
    let unitPesoDolar = 98.15
    let unitPesoEuro = 99.39
    let unitPesoReal = 14.67
    
    let unitDolarPeso = 0.016
    let unitDolarEuro = 1.08
    let unitDolarReal = 0.23
    
    let unitEuroPeso = 0.015
    let unitEuroDolar = 0.93
    let unitEuroReal = 0.21
    
    let unitRealPeso = 0.071
    let unitRealDolar = 4.37
    let unitRealEuro = 4.71
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        resuLabel.text = ""
        
        montoIngreso.delegate = self
  //      self.ocultarTecladoCuandoTocoAfuera()
        
    }
    
    
    
    @IBAction func miResultado(_ sender: UIButton) {
        
        
        // el valor ingresado es double a la fuerza, tambien el ingreso de dato
        let miValor = Double(montoIngreso.text!)!
        
        
        // el segmento se añade a una variable
        let ingOrigen = monOrigen.selectedSegmentIndex
        
        let ingDestino = monDestino.selectedSegmentIndex
        
        
        
        if ingOrigen == 0 && ingDestino == 0{
            let valorConvert = miValor * 1
            // valorConvert es la variable que contiene el resultado
            dosDecimales(double: valorConvert)
            view.endEditing(true)
        }else{
            if ingOrigen == 0 && ingDestino == 1{
                let valorConvert = miValor / unitPesoDolar
                dosDecimales(double: valorConvert)
                view.endEditing(true)
        }else{
            if ingOrigen == 0 && ingDestino == 2{
                let valorConvert = miValor / unitPesoEuro
                dosDecimales(double: valorConvert)
                view.endEditing(true)
        }else{
            if ingOrigen == 0 && ingDestino == 3{
                let valorConvert = miValor / unitPesoReal
                dosDecimales(double: valorConvert)
                view.endEditing(true)
            }else{
                if ingOrigen == 1 && ingDestino == 0{
                    let valorConvert = miValor / unitDolarPeso
                    dosDecimales(double: valorConvert)
                    view.endEditing(true)
            }else{
                if ingOrigen == 1 && ingDestino == 1{
                    let valorConvert = miValor * 1
                    dosDecimales(double: valorConvert)
                    view.endEditing(true)
            }else{
                if ingOrigen == 1 && ingDestino == 2{
                    let valorConvert = miValor / unitDolarEuro
                    dosDecimales(double: valorConvert)
                    view.endEditing(true)
            }else{
                    if ingOrigen == 1 && ingDestino == 3{
                        let valorConvert = miValor / unitDolarReal
                        dosDecimales(double: valorConvert)
                        view.endEditing(true)
                }else{
                    if ingOrigen == 2 && ingDestino == 0{
                        let valorConvert = miValor / unitEuroPeso
                        dosDecimales(double: valorConvert)
                        view.endEditing(true)
                }else{
                    if ingOrigen == 2 && ingDestino == 1{
                        let valorConvert = miValor / unitEuroDolar
                        dosDecimales(double: valorConvert)
                        view.endEditing(true)
                }else{
                    if ingOrigen == 2 && ingDestino == 2{
                        let valorConvert = miValor * 1
                        dosDecimales(double: valorConvert)
                        view.endEditing(true)
                }else{
                    if ingOrigen == 2 && ingDestino == 3{
                        let valorConvert = miValor / unitEuroReal
                        dosDecimales(double: valorConvert)
                        view.endEditing(true)
                    }else{
                        if ingOrigen == 3 && ingDestino == 0{
                            let valorConvert = miValor / unitRealPeso
                            dosDecimales(double: valorConvert)
                            view.endEditing(true)
                    }else{
                        if ingOrigen == 3 && ingDestino == 1{
                            let valorConvert = miValor / unitRealDolar
                        //    dosDecimales(convertidoEs: valorConvert)
                            
                            let formatter = NumberFormatter()
                            formatter.numberStyle = .decimal
                            formatter.maximumFractionDigits = 0

                            let result = formatter.string(from: NSNumber(value: valorConvert))
                            
                            resuLabel.text = "\(String(describing: result!)) Dolares"
                            
                            view.endEditing(true)
                    }else{
                        if ingOrigen == 3 && ingDestino == 2{
                            let valorConvert = miValor / unitRealEuro
                            dosDecimales(double: valorConvert)
                            view.endEditing(true)
                    }else{
                        if ingOrigen == 3 && ingDestino == 3{
                            let valorConvert = miValor * 1
                            dosDecimales(double: valorConvert)
                            view.endEditing(true)
                            }
                        }
                    }
                }
            }
                }
                    }
                        }
                    }
                }
            }
                }
                    }
                        }
                    }
                }
            }

    
    
        

    
    // funcion para imprimir con 2 decimales
    
    func dosDecimales(double convertidoEs : Double){
        let endValue = String(format: "%.2f", convertidoEs)
      
        
        if monOrigen.selectedSegmentIndex == 0 && monDestino.selectedSegmentIndex == 0 || monOrigen.selectedSegmentIndex == 1 && monDestino.selectedSegmentIndex == 0 || monOrigen.selectedSegmentIndex == 2 && monDestino.selectedSegmentIndex == 0 || monOrigen.selectedSegmentIndex == 3 && monDestino.selectedSegmentIndex == 0{
            resuLabel.text = "\(endValue) Pesos"
        }else{
            if monOrigen.selectedSegmentIndex == 0 && monDestino.selectedSegmentIndex == 1 || monOrigen.selectedSegmentIndex == 1 && monDestino.selectedSegmentIndex == 1 || monOrigen.selectedSegmentIndex == 2 && monDestino.selectedSegmentIndex == 1 || monOrigen.selectedSegmentIndex == 3 && monDestino.selectedSegmentIndex == 1{
                resuLabel.text = "\(endValue) Dólares"
            }else{
                if monOrigen.selectedSegmentIndex == 0 && monDestino.selectedSegmentIndex == 2 || monOrigen.selectedSegmentIndex == 1 && monDestino.selectedSegmentIndex == 2 || monOrigen.selectedSegmentIndex == 2 && monDestino.selectedSegmentIndex == 2 || monOrigen.selectedSegmentIndex == 3 && monDestino.selectedSegmentIndex == 2{
                    resuLabel.text = "\(endValue) Euros"
                }else{
                    if monOrigen.selectedSegmentIndex == 0 && monDestino.selectedSegmentIndex == 3 || monOrigen.selectedSegmentIndex == 1 && monDestino.selectedSegmentIndex == 3 || monOrigen.selectedSegmentIndex == 2 && monDestino.selectedSegmentIndex == 3 || monOrigen.selectedSegmentIndex == 3 && monDestino.selectedSegmentIndex == 3{
                        resuLabel.text = "\(endValue) Reales"
                    }
     
                        }
                    }
                }
            }


    
    
}

// Esconder teclado con ENTER

extension UIViewController : UITextFieldDelegate {
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

// Al terminar agregar como delegados a nosotros de presionar ENTER: self.TEXTFIELD.delegate = self

// Esconder teclado cuando toco la pantalla

extension UIViewController {
    
    func ocultarTecladoCuandoTocoAfuera(){
        let tap : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.ocultarTeclado))
        view.addGestureRecognizer(tap)
    }
    
    @objc func ocultarTeclado(){
        view.endEditing(true)
    }
    
    
    
}
